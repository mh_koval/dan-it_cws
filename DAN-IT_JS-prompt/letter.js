let toWhom = prompt('Кому?', 'Воронову А.В.');
let letterDate = prompt ('Дата письма', '18.07.2017')||'';
let fullName = prompt('Полное имя', 'Андрей Викторович');
let dateFrom = prompt('От какого числа?', '17.07.2017');
let fromWho = prompt('От кого?', 'Петрова О.Л.');
let fromPos = prompt('Должность', 'Директор')||'';

document.write(`
<div class="wrapper" style="width: 80%; margin: 0 auto; line-height: 1.5; text-align: justify">
    <div class="container1">
        <div class="toWhom" style="float: right; margin-top: 20px;">
                <p>Генеральному директору ООО "Пион"</p>
                <p>${toWhom}</p>
        </div>
    </div>
    <div class="clearfix" style="clear: both; visibility: hidden">.</div>
    <div class="container2">
        <div class="letterDate" style="float: left; margin-top:20px">${letterDate}г. №3</div>
    </div>
    <div class="clearfix" style="clear: both; visibility: hidden">.</div>
    <div class="container3">
    <p style="text-align: center; margin-top: 50px">Уважаемый ${fullName}!</p>
    </div>
    <div class="text">
    <p>Высылаем на ваш адрес, подписанный и скрепленный печатью договор на изготовление наружной рекламы № 1 от ${dateFrom}г. Просим его подписать, скрепить печатью и один экземпляр направить нам в течение семи дней.</p>
    <p>Приложение: договор в двух экземплярах (всего на 8 листах).</p>
    </div>
    <div class="position" style="display: flex; justify-content: space-between">
        <p>${fromPos}</p>
        <p style="font-style: italic;">Петрова</p>
        <p>${fromWho}</p>
    </div>
</div>
`);





